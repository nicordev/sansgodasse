---
layout: post
title: "Balade de 4 jours dans le massif de Belledonne"
author: sansgodasse
---

C'est parti pour découvrir le massif de Belledonne pendant 4 jours!

Comme je n'ai pas d'entrainement, que je me sens bien fatigué et que je n'ai jamais dormi à la belle étoile en pleine montagne, je me dis que je verrais bien jusqu'où je pourrais aller, au pire je passe mon temps à méditer au milieu de la montagne, voir je rentre plus tôt.

Au passage j'en profite pour tester la randonnée sans manger que j'ai vu sur le site de Florian Gomet, [jeûne et trek](https://freeyourlife.fr/trek-et-jeune/), du coup jeudi soir c'est purge au sel de nigari et vendredi midi c'est jus de légumes et repas de légumes cuits à la vapeur douce avant de prendre la route, 36 degrés dans la voiture sans clim pendant plus de 2 heures, j'ai droit à un sauna gratuit pour évacuer encore d'avantage de toxines!

Arrivé vendredi soir au parking de Gleyzin avec des averses de pluies, petite marche en sandales puis pieds nus pour trouver un endroit où dormir dans les bois, je trouve une zone plutôt plate au pied d'un conifère, je trouve 3 branches mortes assez longues et solides pour faire un trépied avec de la ficelle, j'enfile mon poncho dans le trépied pour me faire un abris pour le haut du corps, j'installe mon sac de couchage dans un sur-sac imperméable et je place mon sac à dos dans un sac poubelle, me voilà prêt pour la nuit!

L'orage gronde juste au-dessus du bois toute la première partie de la nuit, il pleut beaucoup et pour autant mon abri fonctionne bien et je reste au sec! Au petit matin je me réveille en bonne forme, mon système ayant bien fonctionné, je prends 3 branches mortes plus petites dans mon sac à dos pour pouvoir me faire un autre abri si besoin.

![abri](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_072201.jpg)

Samedi marche pieds nus jusqu'au refuge de l'ourle où j'achète un pic-nic que je mangerai le lendemain pour ma reprise alimentaire et où je demande quelques renseignements sur le secteur, puis je repars pieds nus en direction du col de comberousse avec comme objectif le Puy gris.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_090320_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_091713.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_091707_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_120913.jpg)

J'avance lentement et avec plaisir au milieu des gros rochers, un peu trop lentement d'ailleurs vu que j'arrive trop tard au col de comberousse pour pouvoir atteindre le sommet du Puy gris sereinement, du coup je renonce au sommet, j'enfile mes chaussures minimalistes un peu avant le col pour pouvoir me déplacer plus rapidement et une fois arrivé au col, je découvre un chamois qui mange tranquillement et ne semble pas inquiété par ma présence.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_151400_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_162343_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_170619_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_172848.jpg)

Je descends dans la vallée pour rejoindre le lac noir avant la nuit. Arrivé sur un névé, j'utilise une des branches que j'avais récupéré pour faire ma première descente en ramasse, c'est marrant! 

Au passage je fais la connaissance d'un couple de randonneurs super sympa avec qui je partage ma découverte des purges et de la randonnée sans manger, et je découvre une dizaine de bouquetins mâles avec leurs grandes cornes à proximité du lac noir.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_174604.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_193151_1.jpg)

J'installe mon sac de couchage à proximité du lac, pas d'orage en vue, alors je ne fais pas d'abri pour le haut du corps. Avant de me coucher, je fait une série de respiration de la méthode Wim Hof puis je me baigne quelques minutes dans le lac noir où, après avoir passé l'inconfort lié au froid, je sens un profond sentiment de sérénité, seul dans le lac noir au milieu de la montagne et des bouquetins.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_202751.jpg)

La nuit est magique, lorsque je me réveille par moment je vois la silhouette des montagnes, au-dessus les étoiles et parfois des étoiles filantes, j'entends quelques pierres dévaler la pente au loin puis je me rendors.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220806_211554.jpg)

Dimanche marche jusqu'au refuge de l'ourle en passant par la croix du Léat, fin du jeune à 10h où je mange des myrtilles et la salade du pic-nic, puis je mange un sandwich vers 16h arrivé à la pierre du pin. Une fois au refuge, je décide d'y passer la nuit pour découvrir ce que c'est que de dormir en refuge, et je fais la connaissance d'autres randonneurs bien sympa avec qui je passe une bonne soirée, par contre j'ai plus de mal pour m'endormir dans le refuge que lorsque j'étais au lac noir, car certes il fait plus chaud dans le refuge qu'au bord du lac, mais au moins les bouquetins ne ronflaient pas eux!

Bref, lundi matin j'ai toujours la forme, alors je prends la direction du col morétan en sandales que j'atteinds vers midi, puis je descends de l'autre côté pour atteindre le lac supérieur un peu avant 14h. J'en profite pour m'assoir un moment dans l'eau du lac, encore plus froide qu'au lac noir! Puis je remonte au col morétan où je reste une heure seul à profiter du calme et de la vue, quand soudain un chamois vient passer un peu plus haut sans me voir pour descendre vers le lac.

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_115809_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_115829_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_134957_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_152321_1.jpg)

Ensuite je descends en direction du refuge de l'ourle et je m'arrête une centaine de mètres en amont, sur une petite plateforme de mousse entourée de myrtilles, l'endroit idéal pour une dernière nuit à la belle étoile d'où je peux admirer le coucher de soleil au loin sur le massif du Vercors, allongé en mangeant des myrtilles, magique!

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_194303_1.jpg)
![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220808_204644_1.jpg)

Mardi matin je redescends au parking de Gleyzin en passant par le chalet du bout qui a été rénové par l'association [tous à poêle](https://www.tousapoele.org/), le soleil est toujours présent, arrivé au parking je me sens en bien meilleure forme que 4 jours plus tôt et je me dis que je reviendrais bien dans le coin!

![](https://filedn.eu/l7h9zlmsxqsJ1zcaE4NssGf/202208_belledonne/IMG_20220809_130405_1.jpg)