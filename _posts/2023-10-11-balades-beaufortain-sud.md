---
layout: post
title: "Balades dans le sud du Beaufortain"
author: sansgodasse
---

Arrivé au gîte samedi vers 15h, j'en profite pour faire un petit tour à la cabane de Lachat où je vais passer la nuit dans mon hamac, en entendant le brâme des cerfs.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231007_lachat.jpg" | absolute_url }}" alt="Cabane de Lachat" />
        </div>
        <figcaption>Cabane de Lachat</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231007_sunset_forest.jpg" | absolute_url }}" alt="Coucher de soleil dans les bois" />
        </div>
        <figcaption>C'est parti pour une bonne nuit</figcaption>
    </div>
</figure>

Dimanche matin je suis réveillé par la musique du départ d'un trail, j'en profite pour finir la nuit un peu plus haut et admirer le lever du soleil.

Ensuite je monte jusqu'au belvédère situé au-dessus du roc Marchand. De là-haut j'admire les possibilités qui s'offrent à moi pour les prochains jours, et au moment de partir je vois un cerf à environ 75 m qui marche paisiblement, inoubliable.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231008_cerf.jpg" | absolute_url }}" alt="Cerf au loin" />
        </div>
        <figcaption>Un cerf et le Mont Blanc sont visibles sur cette image, si si</figcaption>
    </div>
</figure>

Sur la descente je discute avec des membres de l'organisation du trail qui me donnent des tuyaux pour la suite, bien sympa, puis arrivé au gîte je me prépare pour ma prochaine balade, préparation du sac à dos, fin du repas à la vapeur douce vers 14h, une nuit pas terrible avec le lampadaire de la rue qui reste allumé toute la nuit, et c'est parti pour se balader en montagne !

Lundi départ à 7h34, l'idée de départ est de rejoindre le refuge de la Coire pour y passer la nuit, puis de faire une petite boucle autour du crêt du Rey en alternant pieds nus, sandales et chaussures minimalistes selon le terrain et le ressenti du moment, le tout sans manger.

La montée se fait bien, au fil du temps je sens de moins en moins la faim (alors que je me sens toujours affamé d'ordinaire). Lors des pauses j'en profite pour boire un peu d'eau et je fais 5 minutes de cohérence cardiaque.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_roc_marchand.jpg" | absolute_url }}" alt="Vue de la crête de la veille" />
        </div>
        <figcaption>Vue de la crête où j'étais la veille</figcaption>
    </div>
</figure>

> Sur le chemin je passe par une petite cabane à Beauregard qui est ouverte, bon à savoir si j'ai besoin d'y passer la nuit au retour.

Arrivé au col des tufs blancs à 12h, j'en profite pour m'entrainer à installer mon poncho en mode mini-tente et faire une sieste. C'est un peu étroit, mais ça devrait le faire pour passer une nuit si besoin.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_col_tufs_blancs.jpg" | absolute_url }}" alt="Col des tufs blancs" />
        </div>
        <figcaption>Col des tufs blancs</figcaption>
    </div>
</figure>

Je repars, je me trompe de chemin et je passe finalement par un col entre pierre percée et le crêt du Rey, pour rejoindre le col de la grande combe, en voyant 2 marmottes.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_pierre_percee.jpg" | absolute_url }}" alt="Passage à proximité de pierre percée" />
        </div>
        <figcaption>Passage à proximité de pierre percée</figcaption>
    </div>
</figure>

Je rejoins le cormet d'Arêches à 15h25, après m'être cogné plusieurs fois les pieds contre les bords du sentier qui s'est creusé par le passage des randonneurs, le sentier paraît facile, du coup baisse de vigilance et bim ! Retour à la réalité : pieds nus le moindre choc contre les bords ou contre des pierres se fait vigoureusement sentir. Bon j'arrive toujours à marcher, ça devrait le faire.

Arrivé au [refuge de la Coire](https://refugedelacoire.fr), je dépose le paiement de ma nuité dans la boîte dédiée (15 €), et je me rends compte que j'ai encore du temps devant moi et toujours plein d'énergie ! Du coup je pars rejoindre la croix du Berger, en chaussure pour soulager mes orteils cognés.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_refuge_coire.jpg" | absolute_url }}" alt="Arrivé vers le refuge de la Coire" />
        </div>
        <figcaption>Arrivé vers le refuge de la Coire</figcaption>
    </div>
</figure>

J'arrive à 17h16 à la croix du Berger, le point de vue est splendide ! J'y reste jusqu'au couché du soleil, en compagnie d'un troupeau de chèvres en contre-bas, puis je redescends au refuge pour mettre mes pieds dans l'eau froide, ça détend...

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_croix_berger.jpg" | absolute_url }}" alt="Arrivé à la croix du berger" />
        </div>
        <figcaption>Arrivé à la croix du berger</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231009_sunset_croix_berger.jpg" | absolute_url }}" alt="Coucher de soleil à la croix du berger" />
        </div>
        <figcaption>Coucher de soleil à la croix du berger</figcaption>
    </div>
</figure>

Je dors seul dans le refuge, et pourtant je me réveille souvent. Les orteils de mon pieds droit sont douloureux, bref, encore une nuit pas terrible...

Mardi je me lève à 7h15 et j'admire le lever de soleil, puis je fais mon sac et c'est reparti, direction le col de Corne Noire !

> Je profite du froid matinal pour marcher en sandales, ça protège un peu mes orteils tout en permettant au froid de servir d'anti-inflammatoire pour mes orteils. Quelques heures plus tard la douleur a disparue.

J'aperçois la silouette d'un chamois sur une crête, puis plus tard un groupe de 9 chamois sous le col de Corne Noire, que j'atteins à 10h09. Une fois là-bas, je m'engage sur le sentier qui mène à la pointe de Combe Bénite pour avoir une meilleure vue sur la suite du chemin vers le chalet des veaux du fruit, et finalement je décide de monter jusqu'au sommet, qui comprends un passage un peu exposé.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231010_col_corne_noire_sign.jpg" | absolute_url }}" alt="Col de corne noire" />
            <img src="{{ "/assets/blog/images/post6_20231010_col_corne_noire.jpg" | absolute_url }}" alt="Col de corne noire" />
        </div>
        <figcaption>Col de corne noire</figcaption>
    </div>
</figure>

Arrivé au sommet à 11h, je suis rejoins par un parapentiste qui allait décoller d'ici, super idée !

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231010_pointe_combe_benite.jpg" | absolute_url }}" alt="Pointe de combe bénite" />
            <img src="{{ "/assets/blog/images/post6_20231010_pointe_combe_benite_vue_mont_blanc.jpg" | absolute_url }}" alt="Pointe de combe bénite" />
        </div>
        <figcaption>Pointe de combe bénite</figcaption>
    </div>
</figure>

Finalement je ne l'ai pas vu décoller, le vent n'était pas favorable et le temps passait, je suis redescendu vers le chalet des veaux puis vers le chalet de la Plagne où des bergers fabriquent le fromage Beaufort.

> Pour info la carte IGN indique *Refuge communal de la Plagne*, alors que ce n'est pas un refuge mais bien un chalet privé dédié à la fabrique du Beaufort.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231010_chalet_la_plagne.jpg" | absolute_url }}" alt="Chalet la Plagne" />
        </div>
        <figcaption>Chalet la Plagne</figcaption>
    </div>
</figure>

Il est 12h16, je remplis ma gourde, discute un peu avec les bergers et demande mon chemin pour atteindre la pointe de la Chauvière (aussi appelée pointe du Dzonfié), vu que je me sens en pleine forme !

J'atteins la pointe du Dzonfié à 14h09 où je rencontre Lulu, un des organisateurs du trail que j'ai rencontré samedi, une belle surprise ! Il me raconte ses expéditions au Népal en utilisant [l'aéroport de Lukla](https://www.youtube.com/watch?v=1_0_BT10gEs) pour ensuite se promener au milieu de sommets de plus 6000 m, c'est encore un autre monde... Au passage, vu que là-bas la majorité des locaux sont pieds-nus, ça pourrait être sympa de faire leur rencontre.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231010_pointe_chauviere.jpg" | absolute_url }}" alt="Pointe de la Chauvière" />
        </div>
        <figcaption>Pointe de la Chauvière</figcaption>
    </div>
</figure>

Je continue sur la crête, je commence à grimper le passage "étroit et aérien" comme précisé sur le panneaux sur quelques mètres, puis je repense à ce que m'on dit mes proches : *ne prend pas de risques*, alors je préfère rejoindre le sentier de contournement sur la gauche, nettement moins risqué.

Il est 17h, j'arrive au col du Va en pensant rejoindre la cabane repérée la veille, et je vois écris *Le Quermoz, 10 minutes*... Comme j'ai toujours la forme, je change de plan : je vais passer par le Quermoz et rejoindre le lac Bozon avant la nuit.

> Quel bonheur de pouvoir me promener dans ces montagnes, alors que j'ai failli ne plus jamais remarcher sur 2 jambes, de temps à autre des souvenirs de l'hôpital me reviennent et je ressens un profond sentiment de gratitude.

Arrivé au Quermoz, la vue est toujours aussi magnifique, j'en profite pendant 20 minutes avant de reprendre le sentier vers le lac, où je découvre un parapentiste qui se pose à proximité de l'air de décollage de parapente pour rejoindre son véhicule. On discute un moment tous les 2 puis il me dépose à 1 km de la cabane du crêt Coquet. En marchant vers la cabane je trouve une bute nommée le signale d'où la vue est splendide, alors j'installe mon poncho, j'admire le coucher du soleil et je vais dormir sous les étoiles !

> Comme la nuit s'annonce fraîche à 2045 m d'altitude, j'ai enroulé mon sac de couchage dans une couverture de survie avant de le glisser dans le sur-sac.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231010_sunset_signal.jpg" | absolute_url }}" alt="Le signal" />
        </div>
        <figcaption>Le signal</figcaption>
    </div>
</figure>

La nuit c'est mieux passée que les dernières, je n'ai pas eu froid. Au réveil je découvre du givre sur le poncho et mon sac à dos, j'ai bien fait d'ajouter la couverture de survie !

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231011_poncho_gele.jpg" | absolute_url }}" alt="Poncho en guise d'abri" />
        </div>
        <figcaption>Poncho en guise d'abri</figcaption>
    </div>
</figure>

Il est 7h, je profite du lever du soleil, et je m'y reprends à plusieurs reprises pour ranger mes affaires givrées sans mouiller l'intérieur du sac.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231011_sunrise_signal.jpg" | absolute_url }}" alt="Une belle journée qui s'annonce" />
        </div>
        <figcaption>Une belle journée qui s'annonce</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231011_temporary_backpack.jpg" | absolute_url }}" alt="Sac de couchage enroulé" />
        </div>
        <figcaption>J'opte pour laisser mon sac de couchage dans le sursac, de rouler le tout et de le fixer devant mon sac à dos</figcaption>
    </div>
</figure>

Je pars en direction de Grand Naves et je m'arrête aux Terreaux pour y faire sécher mon matériel et tout remettre dans le sac à dos. J'en profite aussi pour ceuillir des cynorhodons et quelques plantes (plantain lancéolé, pissenlit et gaillet mou).

Enfin, je rentre tranquillement en direction du gîte, content d'avoir pu faire tout ce parcours avec autant d'aisance, alors que je ne fais que très peu de sport ces dernières années. Les balades pieds-nus sans manger me réussissent !

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231011_plantes_recoltees.jpg" | absolute_url }}" alt="Plantes sauvages" />
        </div>
        <figcaption>La cueillette de la matinée</figcaption>
    </div>
</figure>

Jeudi c'est repos au gîte, un bon repas à midi, un coup d'oeil à la météo... Beau temps jusqu'à samedi matin ! Je prépare mon sac et prends la voiture jusqu'au parking de grand maison.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_grandmaison.jpg" | absolute_url }}" alt="Vue depuis le parking" />
        </div>
        <figcaption>Vue depuis le parking</figcaption>
    </div>
</figure>

13h15, j'ai garé la voiture, c'est parti pour marcher au col de la Louze au son du brâme des cerfs du versant d'en face. Bien que le chemin soit aisé, je peine à monter avec la digestion en cours... Mon objectif est d'atteindre les lacs de la tempête et de descendre jusqu'au chalet de Chizeraz pour y passer la nuit au chaud, avec une petite soupe maison que j'emmène avec moi.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_montee_col_louze.jpg" | absolute_url }}" alt="Montée vers le col de la Louze" />
        </div>
        <figcaption>Montée vers le col de la Louze</figcaption>
    </div>
</figure>

Arrivé à un petit torrent à traverser, je quitte les sandales et continue pieds nus. Dans l'ensemble le chemin monte tranquillement et est surtout composé de terre et d'herbes, avec des passages plus caillouteux et quelques petits ruisseaux à traverser, idéal pour ceux qui veulent découvrir la marche pieds nus.

Me voici au col vers 15h, une petite pause pour apprécier la vue et je décide de continuer jusqu'au sommet du Grand Mont. 

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_col_louze.jpg" | absolute_url }}" alt="Col de la Louze" />
        </div>
        <figcaption>Col de la Louze</figcaption>
    </div>
</figure>

Arrivé à mi-parcours, je me rends compte que je n'avance pas assez vite pour pouvoir atteindre le chalet avant la nuit au retour. J'enfile alors mes chaussures et me mets à avancer plus vite dans les rochers. Le sentier présente des passages assez raides et de jolies veine de quartz blanches, c'est joli et amusant, et ça n'en fini pas !

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_montee_grand_mont.jpg" | absolute_url }}" alt="Sur le chemin du Grand Mont" />
        </div>
        <figcaption>Sur le chemin du Grand Mont</figcaption>
    </div>
</figure>

Sommet atteint à 17h, le panorama est spectaculaire ! Tant pis pour l'horaire, je prend le temps de l'admirer avant de redescendre en courant jusqu'au chalet.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_grand_mont.jpg" | absolute_url }}" alt="Le Grand Mont" />
        </div>
        <figcaption>Le Grand Mont</figcaption>
    </div>
</figure>

La descente est fantastique, les rochers sont secs et accrochent bien, les lacs sont splendides et... je perds mon chemin. Je me retrouve à avancer à taton avec le soleil couchant qui éclaire encore le vallon. J'enlève mes chaussures pour traverser les zones humides et je fini par atteindre le chalet à 19h15 avec la nuit qui tombe.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_lac_tempete.jpg" | absolute_url }}" alt="Le lac supérieur" />
        </div>
        <figcaption>Le lac supérieur</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_sunset_barefoot.jpg" | absolute_url }}" alt="Barefoot adventure" />
        </div>
        <figcaption>Barefoot adventure</figcaption>
    </div>
</figure>

À l'intérieur je peux me faire chauffer ma soupe et commencer à installer mes affaires, puis je profite du ciel étoilé un moment avant d'aller me coucher dans un lit du chalet.

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_chalet_chizeraz_poele.jpg" | absolute_url }}" alt="Intérieur du chalet" />
        </div>
        <figcaption>Pour chauffer la soupe</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231012_chalet_chizeraz_interior.jpg" | absolute_url }}" alt="Intérieur du chalet" />
        </div>
        <figcaption>Grand confort</figcaption>
    </div>
</figure>

<figure class="center">
    <div class="superimposed">
        <div>
            <img src="{{ "/assets/blog/images/post6_20231013_chalet_chizeraz_beds.jpg" | absolute_url }}" alt="Intérieur du chalet" />
        </div>
        <figcaption>Il y a même des lits à lattes</figcaption>
    </div>
</figure>

Au matin je repars, le vallon étant orienté vers l'Ouest, il fait un peu froid, et c'est nettement plus difficile de traverser le cours d'eau que la veille.

Vers 9h30 me voici arrivé au lac supérieur qui est maintenant éclairé par le soleil, ça réchauffe, j'en profite pour m'allonger un moment dans le lac, le froid de l'eau fait du bien aux jambes et au dos qui ont été beaucoup sollicité la veille lors de ma descente express.

> Note si vous voulez faire pareil: évitez à tout prix de vous baigner dans des lacs de montagne si vous avez mis de la crême solaire ou tout autre produit sur vous qui peuvent contenir des produits toxiques, vous risquez de polluer le lac et de nuire à la vie aquatique, comme me l'a fait remarqué une maraichère bio que j'ai rencontré quelques jours plus tard. Les tritons notamment y sont très sensibles.

Je repars 1h après en direction du col de la Louze puis du parking, d'où j'entends de nouveaux les cerfs brâmer.

Ce fut une semaine mémorable !

### Détails concernant la cohérence cardiaque

Lorsque je fais ces pauses de 5 minutes, je focalise mon attention sur mon souffle et les pensées se calment, mon corps se détend sans s'endormir, je récupère rapidement et une fois terminé, je repars en marchant de plus belle.

Voici la méthode que j'utilise : [Cohérence cardiaque : TUTO avec Leonardo Pelagotti](https://www.youtube.com/watch?v=nVCyY2Crr5g)

### Détails concernant la purge et le jeune

J'ai découvert les purges avec le livre de Florian Gomet : [guide de survie au 21ème siècle](https://floriangomet.com/produit/guide-de-survie/)

Le dimanche vers 16h j'ai fait un lavement, puis j'ai pris 2 cuillères à soupe de chlorumagène pour purger mes intestins en suivant ce protocole : [https://quotidienmagique.com/le-grand-nettoyage-du-corps-humain/](https://quotidienmagique.com/le-grand-nettoyage-du-corps-humain/)

Le lundi j'ai eu à faire mes besoins, très liquide, du coup facile à enterrer, et une fois dans la nuit. Ensuite une fois le mardi soir et une fois le mercredi matin, au réveil.

J'ai pu constater les effets de la purge sur mon humeur, je me sens plus serein, moins pressé.

Côté douleurs, je n'ai senti quasiment aucune courbature ni crampe et les douleurs liées aux chocs de lundi contre mes orteils ont rapidement disparues.

Le mercredi midi j'ai recommencé à manger en dégustant des myrtilles que j'avais ceuillis en chemin, c'était délicieux !

Puis le mercredi soir j'ai mixé les légumes que j'avais cuit à la vapeur douce avant de partir, pour me faire une soupe improvisée.

### Matériel

- sac à dos wilsa 38 L
- sandales
- chaussures minimalistes
- bâtons de randonnées
- sac de couchage
- sur-sac
- couverture de survie
- tapis de sol
- lacet de chaussure
- poncho
- 6 sardines
- 2 cordelettes et 2 ficelles
- veste imperméable
- veste avec capuche
- T-shirt en coton
- short
- boxer
- paire de chaussettes
- T-shirt à manches longues
- col roulé
- caleçon long
- pantalon léger
- chapeau
- buff (tour de cou)
- gants en cuir
- mouchoir en tissu
- petite serviette
- sachets imperméables
- sachets zippés
- sacs congélation
- 1 sac poubelle 50 L et 2 de 10 L
- sac imperméable transparent
- boussole
- extraits de carte IGN
- crayon de papier avec gomme enroulé dans du ruban adhésif
- jumelles
- gratte langue
- pince tire-tiques
- cuillère
- lame de rasoir
- briquet
- tablettes allume-feu
- coton-tiges
- compresses
- ruban adhésif
- pansements
- serum physiologique
- PQ
- savon
- gourde de 750 mL
- bouteille en plastique de 500 mL
- couteau suisse
- 2 sangles
- mousqueton
- lampe frontale
- fil et aiguille
